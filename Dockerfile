# Dockerfile

FROM wordpress:php8.2

# Install required PHP extensions
# RUN apt-get update && apt-get install -y \
#     php8.2-mysql \
#     php8.2-xml \
#     php8.2-curl \
#     php8.2-gd \
#     php8.2-mbstring \
#     && apt-get clean

# Ensure the correct working directory
WORKDIR /var/www/html
