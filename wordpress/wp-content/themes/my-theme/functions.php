<?php


require_once __DIR__ . '/vendor/autoload.php';

function mytheme_twig_init() {
    $loader = new \Twig\Loader\FilesystemLoader(get_template_directory() . '/templates');
    $twig = new \Twig\Environment($loader, [
        'cache' => get_template_directory() . '/cache',
    ]);

    return $twig;
}

function mytheme_enqueue_styles() {
    wp_enqueue_style('mytheme-style', get_template_directory_uri() . '/assets/css/style.css');
}
add_action('wp_enqueue_scripts', 'mytheme_enqueue_styles');